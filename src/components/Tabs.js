import React, { Component } from 'react';
import { View, Button } from 'react-native';

export default class Tabs extends Component {
    render(){
        return(
              <View style={{ backgroundColor: '#c4c4c4', padding: 10, flexDirection: 'row', justifyContent:'space-between'}}>
                <Button style={{ flex: 1}} onPress={this.props.setScreen(1)} title="Tab 1"/>
                <Button style={{ flex: 1}} onPress={this.props.setScreen(2)} title="Tab 2"/>
              </View>
        );

    }
}


