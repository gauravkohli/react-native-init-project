import React, {Component} from 'react';
import {Platform, StyleSheet, Text, SafeAreaView, TouchableOpacity, View, Button, TextInput, AsyncStorage} from 'react-native';
import styles from './DisplayElementsStyle';
const ELEMENTS_VALUE_KEY = 'ELEMENTS_VALUE_KEY';

export default class DisplayElementsScreen extends Component {
    state = {
        text: '',
        elements: []
    };

    async componentDidMount(){
        const elements = await AsyncStorage.getItem(ELEMENTS_VALUE_KEY);
        this.setState({ elements : JSON.parse(elements) || []});
    }

    navigateTo = (name) => this.props.navigation.navigate('ElementInfo', {name});

    removeElement = (element) => () => this.setState({elements : this.state.elements.filter(elem => elem !== element)}
    );

    render(){
        return (
            <View style={{ flex:1, width: '100%', backgroundColor: 'white'}}>
                {this.state.elements.map((element, index) => (
                    <View key={index} style = {{ padding: 10, flexDirection: 'row', alignItems: 'center' , justifyContent: 'space-between' }}>
                        <TouchableOpacity onPress={this.navigateTo(element)}>
                            <Text  style={{ padding: 10, fontSize: 18, color:'#185185' }}>{element}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.removeElement(element)}>
                            <Text>X</Text>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        );
    }

}

